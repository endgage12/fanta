<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/general.css" rel="stylesheet">
    <title><?= $articleTitle; ?></title>
</head>
<body>
<?php require_once('layout_header.php') ?>
<div class="content">
    <div class="left-content">
        <div class="article">
            <h3><?= $articleTitle ?></h3>
            <br>
            <img src="<?= $articleImg ?>" alt="#">
            <div class="artContent"><?= $articleContent ?></div>
            <br><br>
            <hr>
            <br><br>
        </div>
    </div>
<?php require_once('layout_right_content.php') ?>
</div>
</body>
</html>